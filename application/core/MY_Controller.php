<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	protected function createDirIfNotExist($dirname)
	{
		return is_dir($dirname) || mkdir($dirname, DIR_WRITE_MODE, true);
	}

	protected function trimData($data)
	{
		return trim($str) == null ? null :  $data;
	}

	protected function obfuscate_email($email)
	{
		$em   = explode("@",$email);
		$name = implode('@', array_slice($em, 0, count($em)-1));
		$len  = floor(strlen($name)/2);

		return substr($name,0, 2) . str_repeat('*', strlen($name)-2) . "@" . end($em);   
	}

}