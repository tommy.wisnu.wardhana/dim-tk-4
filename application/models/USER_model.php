<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class USER_Model extends MY_Model {
    
    
    /**
     * Hash password
     */
    public function password_hashes($password)
	{
		$passwd = null;
		$opt = ['cost' => 11];
		$passwd = password_hash($password, PASSWORD_BCRYPT, $opt);
		return $passwd;
    }
    
    /**
     * verifikasi login user
     */
	public function password_verification($username = null, $password = null)
	{
        $result = $this->find_specific_field_where('tuser', 'password', array('username' => $username));
        if(count($result) > 0)
        {
            $hash   =  $result[0]->password;

            if (null != $password)
            {
                $verify = password_verify($password, $hash);
                if($verify)
                    return 1;   //password match
                else
                    return 0;   // password invalid
            }
        }
        else
            return 2;   // tanda bahwa user tidak ditemukan
    }

    // update token ke user yang bersangkutan
    public function update_user_token($username, $token)
    {
        $data = array('token' => $token);
        $this->db->where('username', $username);
        $this->db->update('tuser', $data);
    }

    /**
     * generate data untuk user session
     */
    public function session_generate($username, $level)
    {
        $date = new DateTime();
        $token = hash('sha1', $username .'-'. $date->format('Y-m-d H:i:s'));

        // update token to table user
        $this->update_user_token($username, $token);

        // generate session
        $newdata = array(
            'username'  => $username,
            'token'     => $token,
            'level'     => $level
        );
        
        $this->session->set_userdata($newdata);
    }

    public function session_destroy()
    {
        $newdata = array('username', 'token');
        $this->session->unset_userdata($newdata);
    }

}