<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Auth Lang - English
*
* Author: Ben Edmunds
* 		  ben.edmunds@gmail.com
*         @benedmunds
*
* Author: Daniel Davis
*         @ourmaninjapan
*
* Location: http://github.com/benedmunds/ion_auth/
*
* Created:  03.09.2013
*
* Description:  English language file for Ion Auth example views
*
*/

// Errors
$lang['error_csrf'] = 'This form post did not pass our security checks.';

// Breadcrumbs
$lang['breadcrumb_home']                        = 'Home';
$lang['breadcrumb_master']                      = 'Master';
$lang['breadcrumb_dashboard']                   = 'Dashboard';
$lang['breadcrumb_user_settings']               = 'User Settings';
$lang['breadcrumb_user_settings_user']          = 'Users';
$lang['breadcrumb_user_settings_group']         = 'Groups';
$lang['breadcrumb_log']                         = 'Logs';
$lang['breadcrumb_log_user']                    = 'Users';
$lang['breadcrumb_log_data']                    = 'Datas';
$lang['breadcrumb_settings']                    = 'Settings';


// Common button
$lang['button_save']           = 'Save';
$lang['button_back']           = 'Back';
$lang['button_edit']           = 'Edit';
$lang['button_delete']         = 'Delete';

// Toggle text
$lang['toggle_view']           = 'View Detail';
$lang['toggle_edit']           = 'Edit Data';
$lang['toggle_delete']         = 'Delete ?';

// Login
$lang['login_heading']         = 'Login';
$lang['login_subheading']      = 'Please login with your email/username and password below.';
$lang['login_identity_label']  = 'Email/Username:';
$lang['login_password_label']  = 'Password:';
$lang['login_remember_label']  = 'Remember Me:';
$lang['login_submit_btn']      = 'Login';
$lang['login_forgot_password'] = 'Forgot your password?';

// Index Users
$lang['index_heading']           = 'Users';
$lang['index_subheading']        = 'Below is a list of the users.';
$lang['index_fname_th']          = 'First Name';
$lang['index_lname_th']          = 'Last Name';
$lang['index_email_th']          = 'Email';
$lang['index_groups_th']         = 'Groups';
$lang['index_status_th']         = 'Status';
$lang['index_action_th']         = 'Action';
$lang['index_active_link']       = 'Active';
$lang['index_inactive_link']     = 'Inactive';
$lang['index_create_user_link']  = 'Create a new user';
$lang['index_create_group_link'] = 'Create a new group';

// Deactivate User
$lang['deactivate_heading']                  = 'Deactivate User';
$lang['deactivate_subheading']               = 'Are you sure you want to deactivate the user \'%s\'';
$lang['deactivate_confirm_y_label']          = 'Yes:';
$lang['deactivate_confirm_n_label']          = 'No:';
$lang['deactivate_submit_btn']               = 'Submit';
$lang['deactivate_validation_confirm_label'] = 'confirmation';
$lang['deactivate_validation_user_id_label'] = 'user ID';

// Create User
$lang['create_user_heading']                           = 'Create User';
$lang['create_user_subheading']                        = 'Please enter the user\'s information below.';
$lang['create_user_username_label']                    = 'Username';
$lang['create_user_fname_label']                       = 'First Name:';
$lang['create_user_lname_label']                       = 'Last Name:';
$lang['create_user_company_label']                     = 'Company Name:';
$lang['create_user_identity_label']                    = 'Identity:';
$lang['create_user_email_label']                       = 'Email:';
$lang['create_user_phone_label']                       = 'Phone:';
$lang['create_user_password_label']                    = 'Password:';
$lang['create_user_password_confirm_label']            = 'Confirm Password:';
$lang['create_user_submit_btn']                        = 'Create User';
$lang['create_user_validation_fname_label']            = 'First Name';
$lang['create_user_validation_lname_label']            = 'Last Name';
$lang['create_user_validation_identity_label']         = 'Identity';
$lang['create_user_validation_email_label']            = 'Email Address';
$lang['create_user_validation_phone_label']            = 'Phone';
$lang['create_user_validation_company_label']          = 'Company Name';
$lang['create_user_validation_password_label']         = 'Password';
$lang['create_user_validation_password_confirm_label'] = 'Password Confirmation';

// View User
$lang['view_user_heading']                           = 'View User';
$lang['view_user_subheading']                        = 'Please enter the user\'s information below.';
$lang['view_user_username_label']                    = 'Username';
$lang['view_user_fname_label']                       = 'First Name:';
$lang['view_user_lname_label']                       = 'Last Name:';
$lang['view_user_company_label']                     = 'Company Name:';
$lang['view_user_identity_label']                    = 'Identity:';
$lang['view_user_email_label']                       = 'Email:';
$lang['view_user_phone_label']                       = 'Phone:';
$lang['view_user_password_label']                    = 'Password:';
$lang['view_user_password_confirm_label']            = 'Confirm Password:';
$lang['view_user_submit_btn']                        = 'Create User';
$lang['view_user_validation_fname_label']            = 'First Name';
$lang['view_user_validation_lname_label']            = 'Last Name';
$lang['view_user_validation_identity_label']         = 'Identity';
$lang['view_user_validation_email_label']            = 'Email Address';
$lang['view_user_validation_phone_label']            = 'Phone';
$lang['view_user_validation_company_label']          = 'Company Name';
$lang['view_user_validation_password_label']         = 'Password';
$lang['view_user_validation_password_confirm_label'] = 'Password Confirmation';

// Edit User
$lang['edit_user_heading']                           = 'Edit User';
$lang['edit_user_subheading']                        = 'Please enter the user\'s information below.';
$lang['edit_user_fname_label']                       = 'First Name:';
$lang['edit_user_lname_label']                       = 'Last Name:';
$lang['edit_user_company_label']                     = 'Company Name:';
$lang['edit_user_email_label']                       = 'Email:';
$lang['edit_user_phone_label']                       = 'Phone:';
$lang['edit_user_password_label']                    = 'Password: (if changing password)';
$lang['edit_user_password_confirm_label']            = 'Confirm Password: (if changing password)';
$lang['edit_user_groups_heading']                    = 'Member of groups';
$lang['edit_user_submit_btn']                        = 'Save User';
$lang['edit_user_validation_fname_label']            = 'First Name';
$lang['edit_user_validation_lname_label']            = 'Last Name';
$lang['edit_user_validation_email_label']            = 'Email Address';
$lang['edit_user_validation_phone_label']            = 'Phone';
$lang['edit_user_validation_company_label']          = 'Company Name';
$lang['edit_user_validation_groups_label']           = 'Groups';
$lang['edit_user_validation_password_label']         = 'Password';
$lang['edit_user_validation_password_confirm_label'] = 'Password Confirmation';

//Index Group
$lang['index_heading_group']                = 'Groups';

// Create Group
$lang['create_group_title']                  = 'Create Group';
$lang['create_group_heading']                = 'Create Group';
$lang['create_group_subheading']             = 'Please enter the group information below.';
$lang['create_group_name_label']             = 'Group Name:';
$lang['create_group_desc_label']             = 'Description:';
$lang['create_group_submit_btn']             = 'Create Group';
$lang['create_group_validation_name_label']  = 'Group Name';
$lang['create_group_validation_desc_label']  = 'Description';

// Edit Group
$lang['edit_group_title']                  = 'Edit Group';
$lang['edit_group_saved']                  = 'Group Saved';
$lang['edit_group_heading']                = 'Edit Group';
$lang['edit_group_subheading']             = 'Please enter the group information below.';
$lang['edit_group_name_label']             = 'Group Name:';
$lang['edit_group_desc_label']             = 'Description:';
$lang['edit_group_submit_btn']             = 'Save Group';
$lang['edit_group_validation_name_label']  = 'Group Name';
$lang['edit_group_validation_desc_label']  = 'Description';

// Change Password
$lang['change_password_heading']                               = 'Change Password';
$lang['change_password_old_password_label']                    = 'Old Password:';
$lang['change_password_new_password_label']                    = 'New Password (at least %s characters long):';
$lang['change_password_new_password_confirm_label']            = 'Confirm New Password:';
$lang['change_password_submit_btn']                            = 'Change';
$lang['change_password_validation_old_password_label']         = 'Old Password';
$lang['change_password_validation_new_password_label']         = 'New Password';
$lang['change_password_validation_new_password_confirm_label'] = 'Confirm New Password';

// Forgot Password
$lang['forgot_password_heading']                 = 'Forgot Password';
$lang['forgot_password_subheading']              = 'Please enter your %s so we can send you an email to reset your password.';
$lang['forgot_password_email_label']             = '%s:';
$lang['forgot_password_submit_btn']              = 'Submit';
$lang['forgot_password_validation_email_label']  = 'Email Address';
$lang['forgot_password_identity_label'] = 'Identity';
$lang['forgot_password_email_identity_label']    = 'Email';
$lang['forgot_password_email_not_found']         = 'No record of that email address.';
$lang['forgot_password_identity_not_found']         = 'No record of that username.';

// Reset Password
$lang['reset_password_heading']                               = 'Change Password';
$lang['reset_password_new_password_label']                    = 'New Password (at least %s characters long):';
$lang['reset_password_new_password_confirm_label']            = 'Confirm New Password:';
$lang['reset_password_submit_btn']                            = 'Change';
$lang['reset_password_validation_new_password_label']         = 'New Password';
$lang['reset_password_validation_new_password_confirm_label'] = 'Confirm New Password';



// Create Office
$lang['create_office_heading']                = 'Create Office';
$lang['create_office_saved']                  = 'Office Saved';
$lang['create_office_delete']                 = 'Office Deleted';
$lang['create_office_subheading']             = 'Please enter the group information below.';
$lang['create_office_code_label']             = 'Code';
$lang['create_office_full_code_label']        = 'Full Code';
$lang['create_office_city_label']             = 'City';
$lang['create_office_abbreviation_label']     = 'Abbreviation';
$lang['create_office_description_label']      = 'Description';

// View Office
$lang['view_office_heading']                = 'View Office';

// Edit Office
$lang['edit_office_heading']                = 'Edit Office';



// Index Category
$lang['index_category_heading']            = 'Category List';
// Create Category
$lang['create_category_heading']            = 'Create Category';
$lang['create_category_title_label']        = 'Title';
$lang['create_category_description_label']  = 'Description';
$lang['create_category_saved']              = 'Category Saved';
$lang['create_category_delete']             = 'Category Deleted';

// View Category
$lang['view_category_heading']                = 'View Category';

// Edit Category
$lang['edit_category_heading']                = 'Edit Category';



// Index Disaster
$lang['index_heading_dis']                  = 'Disasters';
$lang['index_dis_title']                    = 'Title';
$lang['index_dis_created_by']               = 'Created By';
$lang['index_dis_created_date']             = 'Created Date';

// Create Disaster
$lang['create_dis_heading']                = 'Create Disaster';
$lang['create_dis_saved']                  = 'Disaster Saved';
$lang['create_dis_delete']                 = 'Disaster Deleted';
$lang['create_dis_information']            = 'Information';
$lang['create_dis_information_location']   = 'Location';
$lang['create_dis_information_date']       = 'Date';
$lang['create_dis_information_results']    = 'Results';
$lang['create_dis_information_attch_title']= 'Information Attachment Title';


// View Disaster

// Edit Disaster


// Index Log Data
$lang['log_data_heading']               = 'Log Data';
$lang['log_data_action']                = 'Action';
$lang['log_data_entity']                = 'Entity';
$lang['log_data_before_save']           = 'Before Save';
$lang['log_data_afftter_save']          = 'After Save';
$lang['log_data_created_by']            = 'Created By';

// View Log Data


// Index Log User
$lang['log_user_heading']               = 'Log User';
$lang['log_user_action']                = 'Action';
$lang['log_user_entity']                = 'Entity';
$lang['log_user_before_save']           = 'Before Save';
$lang['log_user_afftter_save']          = 'After Save';
$lang['log_user_created_by']            = 'Created By';