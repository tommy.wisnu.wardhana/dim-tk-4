<?php $this->load->view('templates_backend/header'); ?>
<?php $this->load->view('templates_backend/menus'); ?>
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <?php $this->load->view($content); ?>
</div>
<?php $this->load->view('templates_backend/footer'); ?>