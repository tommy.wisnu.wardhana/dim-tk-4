
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h5 >Data Perhitungan EOQ</h5>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Data Perhitungan EOQ</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 ">
                <div class="card card-info">
                    <div class="card-header">
                        <h4 class="card-title">Economic Of Order (EOQ)</h4>
                    </div>
                     <div id="infoMessage" style="color:red;padding:5px"><?php echo $message;?></div>
                    <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post" id="frmEOQ">
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="tanggal_pesan" class="col-sm-2 col-form-label">Tanggal</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control"  value="<?php if(isset($existingData->tanggal)) echo $existingData->tanggal?>"  id="tanggal"  name="tanggal" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="tanggal_pesan" class="col-sm-2 col-form-label">Bulan</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="bulan" id="bulan" >
                                        <option value="1">Januari</option>
                                        <option value="2">Februari</option>
                                        <option value="3">Maret</option>
                                        <option value="4">April</option>
                                        <option value="5">Mei</option>
                                        <option value="6">Juni</option>
                                        <option value="7">Juli</option>
                                        <option value="8">Agustus</option>
                                        <option value="9">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="id" class="col-sm-2 col-form-label">ID Barang</label>
                                <div class="col-sm-10">
                                    <select class="form-control select2" style="width: 100%;" name="id_barang" id="id_barang" >
                                        <?php foreach($barangs as $barang): ?>
                                            <option value="<?php echo $barang->id; ?>" <?php if (isset($existingData->id_barang)) { if ($existingData->id_barang == $barang->id ) echo 'selected="selected"' ;}  ?> >[<?php echo $barang->id;?>] <?php echo $barang->nama;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nama" class="col-sm-2 col-form-label">Nama Barang</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?php if(isset($existingData->nama_barang)) echo $existingData->nama_barang?>" id="nama_barang" name="nama_barang" placeholder="Nama Barang">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="harga_beli" class="col-sm-2 col-form-label">Harga Beli</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php if(isset($existingData->harga_beli)) echo $existingData->harga_beli?>" id="harga_beli"  name="harga_beli" placeholder="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="harga_jual" class="col-sm-2 col-form-label">Harga Pesan</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php if(isset($existingData->harga_pesan)) echo $existingData->harga_pesan?>" id="harga_pesan" name="harga_pesan" placeholder="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jumlah" class="col-sm-2 col-form-label">Biaya Pesan</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php if(isset($existingData->biaya_pesan)) echo $existingData->biaya_pesan?>" id="biaya_pesan"  name="biaya_pesan" placeholder="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="biaya_pesan" class="col-sm-2 col-form-label">Biaya Simpan</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php if(isset($existingData->biaya_pesan)) echo $existingData->biaya_simpan?>" id="biaya_simpan" name="biaya_simpan" placeholder="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lead_time" class="col-sm-2 col-form-label">Lead Time</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php if(isset($existingData->lead_time)) echo $existingData->lead_time?>" id="lead_time" name="lead_time" placeholder="0">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lead_time" class="col-sm-2 col-form-label">Permintaan</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php if(isset($existingData->permintaan)) echo $existingData->permintaan?>" id="permintaan" name="permintaan" placeholder="0">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lead_time" class="col-sm-2 col-form-label">EOQ</label>
                                <div class="col-sm-10">
                                <input type="number" readonly class="form-control" value="<?php if(isset($existingData->eoq)) echo $existingData->eoq?>" id="eoq" name="eoq" placeholder="0">
                                </div>
                            </div>

                            <!-- <div class="form-group row">
                                <label for="lead_time" class="col-sm-2 col-form-label">ROP</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php if(isset($existingData->rop)) echo $existingData->rop?>" id="rop" name="rop" placeholder="0">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lead_time" class="col-sm-2 col-form-label">Total Biaya</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php if(isset($existingData->total_biaya)) echo $existingData->total_biaya?>" id="total_biaya" name="total_biaya" placeholder="0">
                                </div>
                            </div> -->
                          
                        
                        </div>

                        <div class="card-footer">
                            <button type="button" onclick="submitForm()" class="btn btn-info">Simpan</button>
                            <a href="<?php echo base_url();?>eoq" class="btn btn-default">Batal</a>
                        </div>
                    
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>

        $( document ).ready(function() {
            $('#tanggal').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' });

            $('.select2').select2();

            $('#id_barang').on('change', function() {
                getInfoBarang(this.value );
            });

            function getInfoBarang(idBarang)
            {
                $.getJSON("<?php echo site_url('eoq/get_data_barang/')?>" + idBarang, function(result){
                    if (result)
                    {
                        $('#nama_barang').val(result.nama);
                        $('#harga_beli').val(result.harga_beli);
                        $('#harga_pesan').val(result.harga_jual);
                        $('#biaya_pesan').val(result.biaya_pesan);
                        $('#biaya_simpan').val(result.biaya_simpan);
                        $('#lead_time').val(result.lead_time);
                    }
                });
            }
            getInfoBarang($('#id_barang').val());

            $("#permintaan").change(function(){
                hitungEOQ();
            });

            $("#biaya_pesan").change(function(){
                hitungEOQ();
            });

            $("#biaya_simpan").change(function(){
                hitungEOQ();
            });   
        });
        
        function hitungEOQ()
        {
            var biaya_pesan = $('#biaya_simpan').val();
            if (isNaN(biaya_pesan)){
                alert("Biaya penyiapan kosong atau bukan angka");
                return;
            }
            if (biaya_pesan < 1){
                alert("Biaya penyiapan tidak boleh 0");
                return;
            }

            var biaya_simpan = $('#biaya_simpan').val();
            if (isNaN(biaya_simpan)){
                alert("Biaya penyimpanan kosong atau bukan angka");
                return;
            }
            if (biaya_simpan < 1){
                alert("Biaya penyimpanan tidak boleh 0");
                return;
            }

            var permintaan = $('#permintaan').val();
            if (isNaN(biaya_simpan)){
                alert("Permintaan kosong atau bukan angka");
                return;
            }
            if (permintaan < 1){
                alert("Permintaan tidak boleh 0");
                return;
            }

            var a = (2 * biaya_pesan * permintaan) / biaya_simpan ;

                var eoq_result =  Math.ceil(Math.sqrt(a));

                $('#eoq').val(eoq_result);
        }

        function submitForm()
        {
            hitungEOQ();
            setTimeout(function(){
                $( "#frmEOQ" ).submit();
            },1000);
        }
    </script>
</section>